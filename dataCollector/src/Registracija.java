import java.awt.*;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Scanner;

public class Registracija extends Thread {


    //path to the folder which will hold recorded data
    public String path = "/home/niki/Desktop/smartFloor/dataCollection2/";

    ///id of the tested person
    public String oseba = "personID_1";

    ///name of the activity
    public String name = "fall";


    public boolean casRegProtokol = true; //condition, to make the 6,7 and 8 recordings longer
    public int interval = 5; //recording interval in seconds
    public int zadnji678 = 10; //recording interval in seconds for recording 6,7,8 IF casRegProtokol = true


    //razni parametri
    public String options = "";
    public int numFiles = 1;

    //za pisanje na file
    public File file = null;
    public FileWriter fw = null;
    public PrintWriter pw = null;

    //progress count
    public int progress = 0;

    public volatile long targetTime = 0;
    public Scanner scan = new Scanner(System.in);

    public Registracija() {
        System.out.println("Data recording program");
    }

    //klicana vsakic, ko zelimo zapisati vrstico
    public void pisiPodatke(String line) {

        if (System.currentTimeMillis() < targetTime) {//registriraj

            pw.println(line);
            pw.flush();

            //graficni prikaz stanja registracije
            progress++;
            if (progress > 10) {
                System.out.print(".");
                progress = 0;
            }
            //System.out.println("Line of text from serial port: " + line);
        }//drugace ne registriraj
    }




    public void run() {


        while (true) {

            file = new File(path + oseba + "/" + name + options + numFiles);

            if (!file.exists()) { //ustvari file


                long regTime = izracunajRegTime();

                System.out.println("Click ENTER to start registering activity num: " + numFiles + " , recording duration: " + (regTime / 1000) + "s " + "   On location: " + path + oseba + "/" + name + options + numFiles);
                scan.nextLine();

                try {
                    file.createNewFile();
                } catch (IOException e) {
                    e.printStackTrace();
                }

                System.out.println("On air.....");
                Toolkit.getDefaultToolkit().beep();

                try { // definiraj writer
                    fw = new FileWriter(file);
                    pw = new PrintWriter(fw);
                } catch (IOException e) {
                    e.printStackTrace();
                }


                //start registracije
                targetTime = System.currentTimeMillis() + regTime;

                try {
                    Thread.sleep(regTime + 1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                System.out.println();
                Toolkit.getDefaultToolkit().beep();

                //zapri writer
                pw.close();

                //interakcija s uporabnikom ob shranjevanju registracije
                interakcija();


            } else { // file ze obstaja inkrementiraj vrednost numFiles in ponovi zanko
                System.out.println("The file already exists");
                numFiles++;
            }


        }
    }

    public long izracunajRegTime() {
        long regTime = interval * 1000;
        ///ce je aktivirana opcija za cas registracije na podlagi protokola se izvede to!
        if (casRegProtokol && (numFiles == 6 || numFiles == 7 || numFiles == 8)) {
            regTime = zadnji678 * 1000;
        }

        return regTime;
    }

    //Vprasa ali se zeli shraniti zbrisati ali ponoviti registracijo
    public void interakcija() {
        //vprasaj ali zelis shraniti
        boolean repeate;

        do {
            System.out.println("Do you want to save the recording? -> y = save     n = delete   r = record again");
            repeate = false;

            char input = scan.nextLine().charAt(0);

            if (input == 'y') {
                numFiles++; //incrementiraj stevilko fila
                System.out.println("Saved");
                options = "";

            } else if (input == 'n') {
                if (file.delete()) {               //returns Boolean value
                    System.out.println(file.getName() + " deleted");   //getting and printing the file name
                } else {
                    System.out.println("failed");
                }

            } else if (input == 'r') {
                System.out.println("Saved, now try to record again");
                options += "r";
            } else {
                repeate = true;
            }


        } while (repeate);
    }


}
