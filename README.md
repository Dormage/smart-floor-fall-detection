[![MIT License][license-shield]][license-url]

<p align="center">
  <a href="https://www.famnit.upr.si/en">
    <img src="img/famnit_logo.png" alt="Logo"  height="80">
  </a>
<a href="https://innorenew.eu/">
    <img src="img/innorenew_logo.jpg" alt="Logo"  height="80">
  </a>
</p>

<!-- ABOUT THE PROJECT -->
## About The Project

The prototype smart fall detection floor was developed as an alternative to existing solutions for indoor location, and fall detection. Typical implementations include but are not limited to wearable devices (i.e., location aware bracelets) that can be discarded by unaware users, or require frequent battery charging, on-site support, and maintenance. Sensor networks that do not rely on wearable devices usually include cameras and microphones coupled with automatic face detection software that have a psychological impact on occupants and raise privacy concerns. 
To overcome these issues, the inventors designed a network of force resistor sensors deployed under the finishing layer of a floor. A high enough density of sensors allows for very accurate location detection, as well as detection of other forced-based phenomena such as falls.

### Hardware requierments
The smart floor is a grid of FRS (forse sensitive resistors), wired to an Arduino controller. The ordering (arduino input) of the sensors is important(grid layout).


<img src="/img/sensor_grid.jpg"  width="300" >


* [FSR](https://www.sparkfun.com/products/9376)
* [Arduino](https://www.sparkfun.com/products/11061)

### Software
The repository contains an Arduino script, which handles the conversion from analogue signal to digital. It periodically writes to the serial port a vector of all sensor values. The interval can be configured in the script before deployment.

The client is a Java FX application that consumes the serial port, and displays the measurements by drawing the grid of tiles, their force, and each individual sensor curve. The application also allows recording tests, which is used to record fall events in an effort to build a representative data-set of falls. The data-set is used as a training set for various AI/ML algorithm for fall detection.


<img src="/img/Gui.png"  width="300" >

<!-- LICENSE -->
## Papers to cite
1. [Tošić, Aleksandar, Jernej Vičič, and Michael David Burnard. Privacy preserving indoor location and fall detection system. Diss. Univerza na Primorskem, Inštitut Andrej Marušič, 2019.](https://www.researchgate.net/profile/Aleksandar_Tosic/publication/341071549_Privacy_preserving_indoor_location_and_fall_detection_system/links/5eabe35d45851592d6ae7ca1/Privacy-preserving-indoor-location-and-fall-detection-system.pdf)

```
    @inproceedings{tovsic2019privacy,
    title={Privacy preserving indoor location and fall detection system},
    author={To{\v{s}}i{\'c}, Aleksandar and Vi{\v{c}}i{\v{c}}, Jernej and Burnard, Michael David},
    booktitle={Human-Computer Interaction in Information Society : proceedings of the 22nd International Multiconference Information Society - IS 2019},
    year={2019},
    pages={9--12},
    url={http://library.ijs.si/Stacks/Proceedings/InformationSociety/2019/IS2019_Volume_H%20-%20HCI.pdf}
    }
    
```



<!-- LICENSE -->
## License

Distributed under the MIT License. See `LICENSE` for more information.



<!-- CONTACT -->
## Contact
Aleksandar Tošić - aleksandar.tosic@upr.si 


Niki Hrovatin - niki.hrovatin@innorenew.eu

Project Link: [https://gitlab.com/Dormage/smart-floor-fall-detection](https://gitlab.com/Dormage/smart-floor-fall-detection)

[license-shield]: https://img.shields.io/github/license/othneildrew/Best-README-Template.svg?style=for-the-badge
[license-url]: https://github.com/othneildrew/Best-README-Template/blob/master/LICENSE.txt
[product-screenshot]: img/sensor_grid.jpg
